%load_ext tensorboard
import pandas as pd 
import glob
import matplotlib.pyplot as plt 
import numpy as np 

import tensorflow_datasets as tfds 
import tensorflow as tf 
import datetime
from tensorflow.keras.callbacks import TensorBoard
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
import seaborn as sns


class UtteraceEncoder(tf.keras.layers.Layer):

    def __init__(self, vocab_size, emb_dim, enc_units, batch_size):
        super(UtteraceEncoder, self).__init__()
        self.batch_size = batch_size
        self.enc_units = enc_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, emb_dim)
        self.gru = tf.keras.layers.GRU(self.enc_units, return_sequences=True, return_state=False, recurrent_initializer='glorot_uniform')

    def call(self, X_utter, hidden):
        x = self.embedding(X_utter)
        output, state = self.gru(x, initial_state = hidden)
        return output, state

    def _initialize_hidden_state(self):
        return tf.zeros((self.batch_size, self.enc_units))


class ContextEncoder(tf.keras.layers.Layer):

    def __init__(self, con_units, batch_size):
        super(ContextEncoder, self).__init__()
        self.batch_size = batch_size
        self.con_units = con_units

    
    def call(self, x, hidden):
        pass

    def _initialize_hidden_state(self):
        return tf.zeros((self.batch_size, self.con_units))


class Decoder(tf.keras.layers.Layer):

    def __init__(self, vocab_size, emb_dim, dec_units, batch_size):
        super(Decoder, self).__init__()
        self.dec_units = dec_units
        self.batch_size = batch_size


    def call(self, x, hidden):
        pass