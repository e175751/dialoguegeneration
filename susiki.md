```math
max V(D) = 
\frac{1}{m}
\sum_{i=1}^{m}
[
\log D
\bigl(
x^{(i)}
\bigr)
+
\log 
\bigl(
1 - D
\bigl(
G
\bigl(
Z^{(i)}
\bigr)
\bigr)
\bigr)
] 
```

```math
min V(G) = 
\frac{1}{m}
\sum_{i=1}^{m}
\log 
\bigl(
1 - D
\bigl(
G
\bigl(
Z^{(i)}
\bigr)
\bigr)
\bigr)
```

```math
L_{1} = 
\sum_{k}
\log P
\bigl(
x_{k} | x_{1}, ... , x_{k-1}; \phi
\bigr)
```


```math
L_{2} = 
\sum_{(x,y)}
\log P
\bigl(
y | x^{1}, ... , x^{m}
\bigr)
```